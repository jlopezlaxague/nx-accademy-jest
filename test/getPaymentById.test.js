const model = require("../model/payments.json");

const { getPaymentById } = require("../controllers/payment.controller");
const { getPayments } = require("../services/payment.service");

jest.mock("../services/payment.service");

beforeEach(() => {
  getPayments.mockClear();
});

describe("Result", () => {
  it("result should be an object if found", () => {
    getPayments.mockReturnValue(model);
    let resp = getPaymentById("4f3fe4eb-6afc-4d7b-b630-10b09177b48c");
    expect(typeof resp).toBe("object");
  });
  it("result should be an falsy if not found", () => {
    getPayments.mockReturnValue(model);
    let resp = getPaymentById("1234");
    expect(resp).toBeFalsy();
  });
});

describe("Execute getPayments", () => {
  it("It should exectue function once", () => {
    getPayments.mockReturnValue(model);
    getPaymentById("1234");
    expect(getPayments.mock.calls.length).toBe(1);
  });
});
