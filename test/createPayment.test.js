const { createPayment } = require("../controllers/payment.controller");
const { validate, create } = require("../services/payment.service");

jest.mock("../services/payment.service");

beforeEach(() => {
  validate.mockClear();
  create.mockClear();
});

describe("validate", () => {
  it("It should exectue validate function once", () => {
    validate.mockReturnValue("ok");
    createPayment();
    expect(validate.mock.calls.length).toBe(1);
  });
});

describe("create", () => {
  it("It should exectue create function once", () => {
    validate.mockReturnValue("ok");
    createPayment();
    expect(create.mock.calls.length).toBe(1);
  });
});
