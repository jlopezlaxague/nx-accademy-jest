const model = require("../model/payments.json");

const { getSummary } = require("../controllers/payment.controller");
const { getPayments } = require("../services/payment.service");

jest.mock("../services/payment.service");

beforeEach(() => {
  getPayments.mockClear();
});

describe("Validate result schema", () => {
  it("result should be an object", () => {
    getPayments.mockReturnValue(model);
    let resp = getSummary();
    expect(typeof resp).toBe("object");
  });
  it("should return two keys", () => {
    getPayments.mockReturnValue(model);
    let resp = getSummary();
    expect(Object.keys(resp).length).toBe(2);
  });
  it("should return keys total and count", () => {
    getPayments.mockReturnValue(model);
    let resp = getSummary();
    expect(Object.keys(resp).includes("total")).toBe(true);
    expect(Object.keys(resp).includes("count")).toBe(true);
  });
  it("should return typeof number values", () => {
    getPayments.mockReturnValue(model);
    let resp = getSummary();
    expect(typeof resp["total"]).toBe("number");
    expect(typeof resp["total"]).toBe("number");
  });
});

describe("Execute getPayments", () => {
  it("It should exectue function once", () => {
    getPayments.mockReturnValue(model);
    getSummary();
    expect(getPayments.mock.calls.length).toBe(1);
  });
});
